// exemple of encapsulation 
let baseSalary = 30000
let overTime = 10
let rate = 20

function getWage(baseSalary, overTime, rate) {
    return baseSalary + (overTime * rate)
}

// VS

let employee = {
    baseSalary : 30000,
    overtime:10,
    rate:20,
    getWage : function () {  
        return this.baseSalary + (this.overtime * this.rate)
    }
}

employee.getWage()

// EXEMPLE of ABSTRACTION
