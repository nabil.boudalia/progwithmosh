function Circle(radius) {
    this.radius = radius
    this.draw = function () {
        console.log('draw')
    }
}

const circle = new Circle(10);

//circle.location = {x:1}

// Enumerating Properies 

//Enumerating all 

for (let key in circle){
    if(typeof circle[key] !== 'function')
    console.log(key, circle[key])
}
// get all keys

const keys = Object.keys(circle)
console.log(keys)

// exitance of object or method
if ('radius' in circle)
    console.log('Circle has a radius')