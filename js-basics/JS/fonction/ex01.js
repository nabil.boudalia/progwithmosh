function sum (...items) {
     
  return items.reduce((a, b) => a + b)
}
