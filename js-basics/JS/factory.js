let address = {
    street:'a',
    city:'b',
    zipCode:'c'
}

// Factory Function 

 function createAddress (street, city, zipCode) {
     return {
         street,
         city,
         zipCode
     }
 }
