// // Factory Function

// function createCircle (radius) {  
//     return {
//         radius,
//         draw() {
//             console.log('draw')
//         }
//     };
// }

// const myCircle = createCircle(1)

// // Constructor Function

// function Circle (radius) {  
//     this.radius = radius;
//     this.draw = function() {
//         console.log ('draw');
//     }
// }
// const another = new Circle(1);
// const x = {};

let obj = {value:10}

function increase(obj) {
    obj.value++
}

increase(obj)
console.log(obj)