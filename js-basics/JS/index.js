
/* ------------BASCIS FUNCTION ------*/

/* function h () {
     document.write(" Hello toi, toi aussi... ")
}
h();
*/
function prenom (iciRemplacePrenom){ 
    // iciRemplacePrenom est le parametre
   document.write(" Hello " + iciRemplacePrenom)
}

prenom("Nabil") 
// nabil c'est l'argument



function prenom (iciRemplacePrenom, iciNom){ // iciRemplacePrenom est le parametre
    console.log(" Bonjour "+ iciRemplacePrenom + " " + iciNom)
 }
 
 prenom("Nabil", "Boudalia") // nabil c'est l'argument


 /*
let dalton =  ["Joe", "William","Jack", "Averell"]

for (i=0; i<=dalton.length;i++){
    console.log([i])
}
*/

/* Performing a task (deplace somethink in console) */

function prenom (iciRemplacePrenom, iciNom){ // iciRemplacePrenom est le parametre
    document.write(" Bonjour " + iciRemplacePrenom + " " + iciNom)
 }
 // calculating a value 

 function square(number) {
    return number * number 
 }

 let number = square(2)
 console.log(number);

 // or 

 console.log(square(2))

